from flask import Flask, jsonify
from flask_cors import CORS
from sense_hat import SenseHat

app = Flask(__name__)
CORS(app)
sense = SenseHat()

temperature = sense.get_temperature()
print("Temperature: %s C" % temperature)

humidity = sense.get_humidity()
print("Humidity: %s %%rH" % humidity)

pressure = sense.get_pressure()
print("Pressure: %s Millibars" % pressure)

@app.route('/temperature')
def getTemperature():
	return str(temperature)

@app.route('/humidity')
def getHumidity():
	return str(humidity)
    
@app.route('/pressure')
def getPressure():
	return str(pressure)
