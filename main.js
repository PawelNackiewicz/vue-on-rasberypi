const currencyAPI = 'https://api.exchangeratesapi.io/latest?base=';
const atmosphericAPI = 'http://127.0.0.1:5000/';

var app = new Vue({
    el: '#app',
    data: {
        today: new Date(),
        actualTime: '',
        tasks: ['napisać sprawozdanie', 'odebrać paczkę', 'zrobić zakupy'],
        quotes: ['Odkładanie na później to największy złodziej. Kradnie Twój czas, sukcesy, cele, życie…'],
        temperature: null,
        humidity: null,
        pressure: null,
        EUR: null,
        USD: null,
        GBP: null,
        newTask: '',
        loading: true,
        errored: false
    },
    filters: {
        currencyDecimal(value) {
            return value.toFixed(2)
        },
        atmosphericDeciaml(value) {
            return value.toFixed(1)
        }
    },
    mounted() {
        this.getExchangeRate('EUR').then(rate => this.EUR = rate);
        this.getExchangeRate('USD').then(rate => this.USD = rate);
        this.getExchangeRate('GBP').then(rate => this.GBP = rate);
        this.getAtmosphericDate('temperature').then(val => this.temperature = val);
        this.getAtmosphericDate('humidity').then(val => this.humidity = val);
        this.getAtmosphericDate('pressure').then(val => this.pressure = val);
    },
    created() {
        setInterval(this.getNow, 1000);
    },
    methods: {
        getExchangeRate(currency) {
            return axios.get(`${currencyAPI}${currency}`)
                .then((response) => {
                    return response.data.rates.PLN;
                })
                .catch((error) => {
                    console.error(error);
                    this.errored = true;
                })
                .finally(() => this.loading = false);
        },
        getAtmosphericDate(param) {
            return axios.get(`${atmosphericAPI}${param}`, {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
                proxy: {
                    host: '127.0.0.1',
                    port: 5000
                }
            })
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    console.error(error);
                })
        },
        getNow: function () {
            const today = new Date();
            this.actualTime = today.getHours().toString().padStart(2, '0') + ":" + today.getMinutes().toString().padStart(2, '0') + ":" + today.getSeconds().toString().padStart(2, '0');
        },
        addNewTask: function () {
            this.tasks.push(newTask.value);
        },
        dropTask: function (task) {
            for (let i = 0; i < this.tasks.length; i++) {
                if (this.tasks[i] === task) {
                    this.tasks.splice(i, 1);
                }
            }
        }
    }
});